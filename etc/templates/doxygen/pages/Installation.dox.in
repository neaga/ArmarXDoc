/**

\page @ARMARX_PROJECT_NAME@-Installation Installation

\tableofcontents

ArmarX can be installed on Ubuntu 14.04 (64 Bit) using Debian packages or by manually compiling all ArmarX packages from source.

\note We recommend using the precompiled ArmarX packages as described below.

\section @ARMARX_PROJECT_NAME@-Installation-Setup-deb The H2T package server

The H2T package server offers precompiled versions of the latest stable ArmarX packages. Additionally, several dependencies, such as Simox and IVT, are available as packages.

\note Currently, only Ubuntu trusty 14.04 (64 Bit) is supported.

Setup the usage of the H2T package server as follows:
\verbatim
curl http://packages.humanoids.kit.edu/h2t-key.pub | sudo apt-key add -
echo -e "deb http://packages.humanoids.kit.edu/trusty/main trusty main\ndeb http://packages.humanoids.kit.edu/trusty/testing trusty testing" | sudo tee /etc/apt/sources.list.d/armarx.list
sudo apt-get update
\endverbatim

\section @ARMARX_PROJECT_NAME@-Dependecies-Installation-deb ArmarX Dependencies - Package Installation

\subsection @ARMARX_PROJECT_NAME@-Dependecies-Installation-standard-deb Dependencies to Standard Ubuntu Packages

The following standard packages from the Ubuntu 14.04 trusty repositories are needed:
\verbatim
sudo apt-get install git gitk git-gui mcpp libeigen3-dev libcoin80-dev python-psutil \
libsoqt4-dev libdb5.1-dev zeroc-ice35 cmake cmake-gui g++ libboost-all-dev \
libdc1394-22-dev doxygen libgraphviz-dev libqwt-dev libpcre3-dev curl libcv-dev libhighgui-dev libcvaux-dev \
mongodb libjsoncpp-dev libssl-dev libv4l-dev python-argcomplete \
libopencv-gpu-dev libopencv-photo-dev libopencv-ts-dev libopencv-superres-dev libopencv-stitching-dev libopencv-videostab-dev \
libgstreamer-plugins-base0.10-dev cppcheck lcov astyle libopencv-dev freeglut3-dev libnlopt-dev libsqlite3-dev
\endverbatim

\subsection @ARMARX_PROJECT_NAME@-Dependecies-Installation-custom-deb Dependencies to Custom Packages

In addition to the dependencies to standard packages, several custom packages are provided via the H2T package server.
If you successfully setup the usage of the H2T package server as described \ref @ARMARX_PROJECT_NAME@-Installation-Setup-deb "here", you can install the needed dependencies as follows:

\verbatim
sudo apt-get install simox ivt ivtrecognition
\endverbatim

To build these dependencies from source, have a look at the following instructions: \ref ArmarXDoc-Dependecies-Installation-Manual "ArmarX Dependencies - Manual Installation"

\section @ARMARX_PROJECT_NAME@-Installation-deb ArmarX - Installation with deb packages

Run following command to install the latest binary ArmarX version. (Ensure that you successfully configured the use of the H2T package server as described in \ref @ARMARX_PROJECT_NAME@-Installation-Setup-deb "The H2T package server"):

\verbatim
sudo apt-get install armarx-dev
\endverbatim

Voilà, now you can continue with the \ref ArmarXCore-Tutorials "ArmarX Tutorials" or create your own \ref armarx-package "ArmarX Package".

\note If you dont have root permissions, you can also \subpage ArmarXCore-Local-Installation "install ArmarX packages locally".

\section @ARMARX_PROJECT_NAME@-Installation-manual ArmarX - Manual Installation

Detailed step-by-step instructions for the installation from source can be found here: \ref ArmarXDoc-ArmarX-Installation-Manual "ArmarX - Installation from source"

\section @ARMARX_PROJECT_NAME@-Installation-GettingStarted Getting Started

After installing Armarx either via packages or from sources you can follow the \ref ArmarXCore-QuickStart "Quick Start" guide and you can start with the \ref @ARMARX_PROJECT_NAME@-Tutorials "ArmarX Tutorials".

\section @ARMARX_PROJECT_NAME@-Installation-packages List of package specific Installation pages
@InstallationList@

\section @ARMARX_PROJECT_NAME@-Installation-from-source Manual Installation Instructions

Detailed step-by-step instructions for the installation from source can be found here: 

\li \subpage ArmarXDoc-Dependecies-Installation "ArmarX Dependencies - Installation from source"
\li \subpage ArmarXDoc-ArmarX-Installation-Manual "ArmarX - Installation from source"
*/
