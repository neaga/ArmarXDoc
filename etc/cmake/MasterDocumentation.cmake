# Setup documentation target for the ArmarX framework

set(SLICE_INCLUDE_FLAGS "")
set(SLICE_FILES "")
set(FOUND_PACKAGES "")

add_custom_target(meta_doc_update)

add_custom_target(addEditLinksToDoc_ArmarXDoc
    COMMAND ${ArmarXCore_BINARY_DIR}/addDoxygenSourceFilesToDoc.py "${CMAKE_BINARY_DIR}" "ArmarXDoc" "${ArmarXDoc_SOURCE_DIR}" "${ArmarXDoc_SOURCE_DIR}/etc/doxygen" "${PROJECT_SOURCECODE_DIR}"
    COMMENT "Adding edit links to to documentation for ${PROJECT_NAME}")

add_dependencies(meta_doc_update addEditLinksToDoc_ArmarXDoc)


# go through all packages found in $PACKAGES_TO_DOCUMENT
# enhance all relevant CMAKE variables related to DOXYGEN_*
# and regenerate the documentation files
foreach (DOC_PACKAGE ${PACKAGES_TO_DOCUMENT})
    if (NOT ${DOC_PACKAGE}_FOUND)
        find_package(${DOC_PACKAGE})
    endif()
    if (${DOC_PACKAGE}_FOUND)
        # store all found packages for later processing in generateCustomPages()
        list(APPEND FOUND_PACKAGES ${DOC_PACKAGE})
        # Doxygen
        message(STATUS "${DOC_PACKAGE} was found: including in documentation")
        set(DOXYGEN_DOCUMENTATION_PAGES_DIR "${DOXYGEN_DOCUMENTATION_PAGES_DIR} ${${DOC_PACKAGE}_DOCUMENTATION_PAGES_DIR}")
        set(DOXYGEN_DOCUMENTATION_SNIPPET_DIR "${DOXYGEN_DOCUMENTATION_SNIPPET_DIR} ${${DOC_PACKAGE}_DOCUMENTATION_SNIPPET_DIR}")
        set(DOXYGEN_DOCUMENTATION_IMAGE_DIR "${DOXYGEN_DOCUMENTATION_IMAGE_DIR} ${${DOC_PACKAGE}_DOCUMENTATION_IMAGE_DIR}")
        set(DOXYGEN_DOCUMENTATION_INPUT_DIR "${DOXYGEN_DOCUMENTATION_INPUT_DIR} ${${DOC_PACKAGE}_DOCUMENTATION_INPUT_DIR}")
        # Slice
        foreach(DEPENDEND_INTERFACE_DIR ${${DOC_PACKAGE}_INTERFACE_DIRS})
            list(APPEND SLICE_INCLUDE_FLAGS "-I${DEPENDEND_INTERFACE_DIR}")
            file(GLOB_RECURSE DOC_SLICE_FILES "${DEPENDEND_INTERFACE_DIR}/*/interface/*.ice")
        endforeach(DEPENDEND_INTERFACE_DIR)
        message(STATUS "DOC_SLICE: ${DOC_SLICE_FILES}")
        list(APPEND SLICE_FILES ${DOC_SLICE_FILES})
        set(SOURCE_DIR_LIST ${${DOC_PACKAGE}_INCLUDE_DIRS})
        separate_arguments(SOURCE_DIR_LIST)
        #string(REPLACE " " ";" SOURCE_DIR_LIST ${${DOC_PACKAGE}_INCLUDE_DIRS})
        list(GET SOURCE_DIR_LIST 0 SOURCE_DIR)
        message(STATUS "source dir: ${SOURCE_DIR}")
        add_custom_target(addEditLinksToDoc_${DOC_PACKAGE}
            COMMAND ${ArmarXCore_BINARY_DIR}/addDoxygenSourceFilesToDoc.py "${CMAKE_BINARY_DIR}" "${DOC_PACKAGE}" "${${DOC_PACKAGE}_SOURCE_DIR}" "${${DOC_PACKAGE}_SOURCE_DIR}/etc/doxygen" "${SOURCE_DIR}"
            COMMENT "Adding edit links to to documentation for ${DOC_PACKAGE}")

        add_dependencies(meta_doc_update addEditLinksToDoc_${DOC_PACKAGE})
    endif()
endforeach()

# remove the current project from the list of packages to document
# otherwise it is included multiple times
list(REMOVE_ITEM FOUND_PACKAGES ${ARMARX_PROJECT_NAME})

function(generateCustomPages)
    set(Doxygen_TEMPLATE_FILES
        Overview
        Tutorials
        HowTos
        FAQ
        GuiPlugins
        #Installation
        #Components
        #Statecharts
        #Packages
        )
    # Always create Installation page    
    configure_file(${PROJECT_TEMPLATES_DIR}/doxygen/pages/Installation.dox.in ${DOXYGEN_DOCUMENTATION_DIR}/Installation.dox @ONLY)
    foreach(template_file ${Doxygen_TEMPLATE_FILES})
        set(${template_file}List "")
        foreach(DOC_PACKAGE ${FOUND_PACKAGES})
            # check if file exists and add an entry
            if (EXISTS "${${DOC_PACKAGE}_ETC_DIR}/doxygen/pages/${template_file}.dox")
                # set template variable
                set(${template_file}List "${${template_file}List}\n\\li \\subpage ${DOC_PACKAGE}-${template_file} \"${DOC_PACKAGE}\"")
            endif()
        endforeach()
        if(NOT "${${template_file}List}" STREQUAL "")
            configure_file(${PROJECT_TEMPLATES_DIR}/doxygen/pages/${template_file}.dox.in ${DOXYGEN_DOCUMENTATION_DIR}/${template_file}.dox @ONLY)
        endif()
    endforeach()
message(STATUS "INPUT: ${DOXYGEN_DOCUMENTATION_INPUT_DIR}")
endfunction()



find_package(Doxygen)
if (DOXYGEN_FOUND)
    message(STATUS "Regenerating Doxygen Documentation files")
    generateCustomPages()
    generateDocumentationFiles()
    generateSliceDocumentation("${SLICE_FILES}" "${SLICE_INCLUDE_FLAGS}")




endif()

