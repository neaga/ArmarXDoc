/**

\page ArmarXDoc-ArmarX-Installation-Manual ArmarX - Installation from source

\tableofcontents

We recommend installing ArmarX via the H2T package server as described here: \ref ArmarXDoc-Installation "ArmarX Installation".
The alternative way of installing ArmarX from sources is described on this page.

\section ArmarXDoc-Installation-Manual-prerequisites ArmarX Prerequesites

Please ensure, that you installed all dependencies according to the following instructions.

- Via the package server:\n
\ref ArmarXDoc-Dependecies-Installation-deb "ArmarX Dependencies - Package Installation"
- Or, by compiling the sources:\n
\ref ArmarXDoc-Dependecies-Installation-Manual "ArmarX Dependencies - Manual Installation"

\section ArmarXDoc-Installation-git-setup Git Setup

Never used Git before? Go and check out the \ref git "Git Tutorials".

If you are using Git for the first time on a computer do the following setup in a shell

\verbatim
git config --global user.name "Your Full Name"
git config --global user.email your.email@domain.example
\endverbatim

If you are lazy (like we all are) and don't want to repeat your username/password
multiple times you should also activate the git credential helpers

\verbatim
git config --global credential.helper cache
\endverbatim

Alternatively you can use credential.helper store, but this is very unsecure since it stores your password in plaintext on your computer.

For further information about credential helpers
please refer to the man page which can be accessed through

\verbatim
man gitcredentials
git help credentials
\endverbatim


\section ArmarXDoc-Installation-obtaining Getting ArmarX
The sourcecode of ArmarX can be fetched by issuing the following commands:

\verbatim
cd ~/home
git clone https://gitlab.com/ArmarX/armarx.git
cd armarx
git submodule update --init
git submodule foreach 'git checkout master'
\endverbatim

This will create a directory named armarx in the home directory of the current user
containing everything which is necessary to get started with ArmarX.
The submodule command will then clone all ArmarX Packages.
In further steps this directory will also be referred to as ${ArmarX_DIR}.

If you are a member of H2T and have access to the internal packages, you should check them out also:
\verbatim
git clone https://i61wiki.itec.uka.de/git/armarx.projects.git
cd armarx.projects
git submodule update --init
git submodule foreach 'git checkout master'
\endverbatim

Please read the \ref ArmarXCore-FAQ-update-armarx "Updating ArmarX guide" if you want to update your ArmarX installation to the latest version.

\subsection ArmarXDoc-Installation-SSH-Push-Access SSH Push Access For Project Members For GitLab

For direct push access to the ArmarX repositories you need to be a member of the ArmarX group on GitLab.
Non members are invited to send us merge requests via GitLab.

Although pushing via HTTPS is possible, it is usually good advice to use <a href="https://gitlab.com/help/ssh/README.md">SSH with private keys</a> instead.

Run the following command in the `armarx` directory to enable push access via SSH for all ArmarX packages:

\verbatim
git submodule foreach 'git config remote.origin.pushurl git@gitlab.com:ArmarX/$name.git'
\endverbatim

\section ArmarXDoc-Installation-compiling Compiling ArmarXCore

After all dependencies are installed the compilation of ArmarXCore can be performed by
executing the following commands:

\verbatim
cd ${ArmarX_DIR}/ArmarXCore/build/
cmake ..
make
\endverbatim

To speed up the build process you should use make with the -j option. To use 4 build processes/cores use:
\verbatim
make -j4
\endverbatim
Note: Each build process uses up to 2GB of ram.

Also we recommend to put the *Core* binary path and *MemoryX* binary path into your PATH environment variable
(Replace ~/home/armarx with the path to your armarx directory):
\verbatim
echo "export PATH=$PATH:~/home/armarx/ArmarXCore/build/bin:~/home/armarx/MemoryX/build/bin" >> ~/.bashrc
. ~/.bashrc
\endverbatim
This allows you to call the armarx tools (e.g. \ref armarx-cli-build, \ref armarx-package) from everywhere.

\section ArmarXDoc-Installation-compiling-projects Compiling all other ArmarX Projects
You have to compile all ArmarX projects.  There are two ways to compile ArmarX packages:

\subsection ArmarX-Compile1 I) Compilation using the ArmarX Build Script

The \ref armarx-cli-build tool retrieves the dependencies of a package automatically.
It is located at {ArmarXCore_DIR}/build/bin/armarx once CMake has been run in ArmarXCore.
The following command compiles everything needed for Armar3:
\verbatim
cd ~/armarx # or whereever your armarx is cloned to
\endverbatim
To change into the armarx base directory for compiling is only needed the first time ArmarX is built.
In subsequent builds the package is findable with CMake.
For the first build, the build tool will look in the current directory and the first level of subdirectories for the packages.
Otherwise it will ask for the paths to each package interactively.
\verbatim
ArmarXCore/build/bin/armarx-dev build RobotSkillTemplates ArmarXSimulation
\endverbatim
or if you extended your PATH variable like described before you can just type:
\verbatim
armarx-dev build RobotSkillTemplates ArmarXSimulation
\endverbatim

\note If you have access to the internal packages you can directly build the Armar3 package instead of RobotSkillTemplates & ArmarXSimulation

The build tool also helps with updating repositories before compiling and other things.
For a full list of available features check the tools dedicated documentation: \ref armarx-cli-build


\subsection ArmarX-Compile2 II) Alternative: Compile each project individually

Repeat the following steps for each of the packages listed below
\verbatim
cd ${ArmarX_DIR}/PROJECTNAME/build/
cmake ..
make
\endverbatim

The following dependency order has to be maintained for Armar3/Armar4
(since dependencies change from time to time it is advised to use the previous compilation approach):
- ArmarXGui
- RobotAPI
- MemoryX
- VisionX
- RobotComponents
- ArmarXSimulation
- RobotSkillTemplates

Internal H2T packages:
- Spoac
- SpeechX
- Armar3
- Armar4




\section ArmarXDoc-Installation-starting-ice How to start Ice and ArmarX Programs

After everything has been compiled you can continue with \ref execution "Starting Ice".

To run bigger examples you should additionally compile other ArmarX Packages.
Please refer to \ref ArmarXDoc-Installation-dependent-packages "Compiling other ArmarX Packages"



\section ArmarXDoc-Installation-testing Running automated tests
Some of the tests require a running instances of IceGrid and IceStorm which can be started
by executing the following script:

\verbatim
cd ${ArmarX_DIR}
./ArmarXCore/build/bin/armarx start
\endverbatim

All tests defined in CMakeLists.txt files can be run using make with the test target as shown below.

\verbatim
cd ${ArmarX_DIR}/ArmarXCore/build
make test
\endverbatim

More information on how the testing is set up can be found on the \ref testing "Testing Setup" page.

*/

